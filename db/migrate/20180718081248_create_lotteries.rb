class CreateLotteries < ActiveRecord::Migration[5.2]
  def change
    create_table :lotteries do |t|
      t.string :rate
      t.boolean :pickable, default: true
      t.belongs_to :lottery_table, foreign_key: true

      t.timestamps
    end
  end
end
