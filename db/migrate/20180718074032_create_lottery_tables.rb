class CreateLotteryTables < ActiveRecord::Migration[5.2]
  def change
    create_table :lottery_tables do |t|
      t.string :title
      t.integer :amount

      t.timestamps
    end
  end
end
