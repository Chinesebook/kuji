class RemoveAmountFromLotteryTables < ActiveRecord::Migration[5.2]
  def change
    remove_column :lottery_tables, :amount
  end
end
