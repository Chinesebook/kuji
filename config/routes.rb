Rails.application.routes.draw do
  resources :lottery_tables do
    resources :lotteries
  end

  root 'lottery_tables#index', as: 'lottery_tables_index'
end
