require 'rails_helper'

RSpec.describe LotteryTable, type: :model do
  describe '#lotteries_builder' do
    let(:table) { LotteryTable.create id: 1, title: :imas }

    context 'when enter the correct parameters' do
      let(:lottery_params) { params = {
                              rate:   ' a   ',
                              amount: 5 } }

      it 'should create lotteries correct' do
        table.lotteries_builder.new(lottery_params)
        table.lotteries_builder.save
        expect(table.lotteries[0].rate).to eq('A')
        expect(table.lotteries.count).to eq(5)
      end
    end

    context 'when enter the wrong parameters' do
      let(:lottery_params) { params = {
                              rate:   'yguhoicxrtcyv',
                              amount: 5 } }

      it 'should raise error message' do
        table.lotteries_builder.new(lottery_params)
        table.lotteries_builder.save
        expect(table.lotteries_builder.errors[:rate]).to include 'is too long (maximum is 5 characters)'
      end
    end
  end
end
