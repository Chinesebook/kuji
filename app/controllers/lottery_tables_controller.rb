class LotteryTablesController < ApplicationController
  def index
    @lottery_tables = LotteryTable.all
  end

  def new
    @lottery_table = LotteryTable.new
  end

  def create
    @lottery_table = LotteryTable.new(lottery_table_params)

    respond_to do |format|
      if @lottery_table.save
        format.html { redirect_to lottery_tables_index_url, notice: 'LotteryTable was successfully created.' }
        format.json { render :show, status: :created, location: @lottery_table }
      else
        format.html { render :new }
        format.json { render json: @lottery_table.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def lottery_table_params
    params.require(:lottery_table).permit(:title)
  end
end
