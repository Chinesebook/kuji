class LotteriesController < ApplicationController
  before_action :set_lottery_table

  def index
    @lotteries = @lottery_table.lotteries.all
  end

  def new
    @lottery = @lottery_table.lotteries.new
  end

  def create
    @lottery_table.lotteries_builder.new(lottery_params)

    respond_to do |format|
      if @lottery_table.lotteries_builder.save
        format.html { redirect_to lottery_table_lotteries_path, notice: 'Lottery was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  private

  def lottery_params
    params.require(:lottery).permit(:rate, :amount)
  end

  def set_lottery_table
    @lottery_table = LotteryTable.find(params[:lottery_table_id])
  end
end
