class LotteryTable < ApplicationRecord

  attr_accessor :lotteries_builder

  has_many :lotteries, dependent: :destroy

  after_initialize :set_lotteries_builder
  # after_find :set_lotteries_builder

  validates :title, presence: true

  private

  def set_lotteries_builder
    @lotteries_builder = LotteriesBuilder.new(self)
  end

end
