class Lottery < ApplicationRecord
  belongs_to :lottery_table

  before_validation :rate_uppercase

  validates :rate, presence: true,
                   length: { in: 1..5 }

  private

  def rate_uppercase
    rate.strip!
    rate.upcase!
  end
end
