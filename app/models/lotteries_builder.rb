class LotteriesBuilder
  include ActiveModel::Model

  attr_accessor :lottery_table
  attr_accessor :lotteries
  attr_accessor :rate

  validates :rate, presence: true,
                   length: { in: 1..5 }

  def initialize(attribute = nil)
    @lottery_table = attribute
    @lotteries = []
  end

  def new(attributes = {})
    @rate = attributes[:rate]
    amount = attributes[:amount].to_i

    @rate.strip!
    @rate.upcase!

    amount.times do
      @lotteries << lottery_table.lotteries.new(rate: @rate)
    end
  end

  def save
    return false unless valid?
    @lotteries.map(&:save)
  end
end
